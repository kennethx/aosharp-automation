﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.IPC;
using AOSharp.Core.UI;
using SmokeLounge.AOtomation.Messaging.Messages;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using static CombatHandler.Generic.PerkCondtionProcessors;

namespace CombatHandler.Generic
{
    public class GenericCombatHandler : AOSharp.Core.Combat.CombatHandler
    {
        private const float PostZonePetCheckBuffer = 5;
        public int EvadeCycleTimeoutSeconds = 180;

        //public static IPCChannel IPCChannel;

        private double _lastPetSyncTime = Time.NormalTime;
        protected double _lastZonedTime = Time.NormalTime;
        protected double _lastCombatTime = double.MinValue;

        //public static IPCChannel IPCChannel = new IPCChannel(0);
        public static Config Config { get; private set; }

        private string pluginDir;

        protected Settings _settings;

        private static double _ncuUpdateTime;

        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();


        public static bool IsActiveWindow => GetForegroundWindow() == Process.GetCurrentProcess().MainWindowHandle;

        protected static HashSet<string> debuffTargetsToIgnore = new HashSet<string>
        {
                    "Immortal Guardian",
                    "Mature Abyss Orchid",
                    "Abyss Orchid Sprout",
                    "Tower of Astodan",
                    "Unicorn Commander Labbe",
                    "Altar of Torture",
                    "Altar of Purification",
                    "Calan-Cur",
                    "Spirit of Judgement",
                    "Wandering Spirit",
                    "Altar of Torture",
                    "Altar of Purification",
                    "Unicorn Coordinator Magnum Blaine",
                    "Xan Spirit",
                    "Watchful Spirit",
                    "Amesha Vizaresh",
                    "Guardian Spirit of Purification",
                    "Tibor 'Rocketman' Nagy",
                    "One Who Obeys Precepts",
                    "The Retainer Of Ergo",
                    "Green Tower",
                    "Blue Tower",
                    "Alien Cocoon",
                    "Outzone Supplier",
                    "Hollow Island Weed",
                    "Sheila Marlene",
                    "Unicorn Advance Sentry",
                    "Unicorn Technician",
                    "Basic Tools Merchant",
                    "Container Supplier",
                    "Basic Quality Pharmacist",
                    "Basic Quality Armorer",
                    "Basic Quality Weaponsdealer",
                    "Tailor",
                    "Unicorn Commander Rufus",
                    "Ergo, Inferno Guardian of Shadows",
                    "Unicorn Trooper",
                    "Unicorn Squadleader",
                    "Rookie Alien Hunter",
                    "Unicorn Service Tower Alpha",
                    "Unicorn Service Tower Delta",
                    "Unicorn Service Tower Gamma",
                    "Sean Powell",
                    "Xan Spirit",
                    "Unicorn Guard",
                    "Essence Fragment",
                    "Scalding Flames",
                    "Guide",
                    "Guard",
                    "Awakened Xan",
                    "Fanatic",
                    "Peacekeeper Coursey",
                    "Harbinger of Pestilence"
        };

        // These are spesific targets that should not be calmed (imune to calms)
        protected static HashSet<string> calmTargetsToIgnore = new HashSet<string>
        {
            "Corrupted Xan-Cur",
            "Corrupted Hiisi Berserker",
            "Corrupted Xan-Kuir",
            "Pandemonium Geosurvey Dog"
        };

        public static HashSet<int> DebuffStopAttack => new HashSet<int>
        {
            302702, // poh, call of rust
            305862, // subway, Treacherous Parrot
        };


        public GenericCombatHandler(string pluginDir)
        {
            this.pluginDir = pluginDir;
            Game.TeleportEnded += TeleportEnded;

            _settings = new Settings("CombatHandler");

            Config = Config.Load($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\Generic\\{Game.ClientInst}\\Config.json");

            //IPCChannel = new IPCChannel(Convert.ToByte(Config.CharSettings[Game.ClientInst].IPCChannel));

            //if (IPCChannel != null)
            //{
            //    IPCChannel.SetChannelId(0);
            //    Chat.WriteLine("set");
            //}
            //else
            //    IPCChannel = new IPCChannel(Convert.ToByte(Config.CharSettings[Game.ClientInst].IPCChannel));

            //Chat.RegisterCommand("channel", (string command, string[] param, ChatWindow chatWindow) =>
            //{
            //    Chat.WriteLine($"Channel set : {param[0]}");
            //    IPCChannel.SetChannelId(Convert.ToByte(param[0]));
            //    Config.CharSettings[Game.ClientInst].IPCChannel = Convert.ToByte(param[0]);
            //    Config.Save();

            //});

            //IPCChannel.RegisterCallback((int)IPCOpcode.RemainingNCU, OnRemainingNCUMessage);

            //IPCChannel.RegisterCallback((int)IPCOpcode.Attack, OnAttackMessage);
            //IPCChannel.RegisterCallback((int)IPCOpcode.StopAttack, OnStopAttackMessage);

            //IPCChannel.RegisterCallback((int)IPCOpcode.Disband, OnDisband);


            RegisterPerkProcessors();
            RegisterPerkProcessor(PerkHash.Limber, Limber, CombatActionPriority.High);
            RegisterPerkProcessor(PerkHash.DanceOfFools, DanceOfFools, CombatActionPriority.High);

            RegisterSpellProcessor(RelevantNanos.FountainOfLife, FountainOfLife);
            RegisterItemProcessor(RelevantItems.FlowerOfLifeLow, RelevantItems.FlowerOfLifeHigh, FlowerOfLife);

            RegisterItemProcessor(RelevantItems.ReflectGraft, RelevantItems.ReflectGraft, ReflectGraft);

            RegisterItemProcessor(RelevantItems.SteamingHotCupOfEnhancedCoffee, RelevantItems.SteamingHotCupOfEnhancedCoffee, Coffee);

            RegisterItemProcessor(RelevantItems.FlurryOfBlowsLow, RelevantItems.FlurryOfBlowsHigh, DamageItem);

            RegisterItemProcessor(RelevantItems.StrengthOfTheImmortal, RelevantItems.StrengthOfTheImmortal, DamageItem);
            RegisterItemProcessor(RelevantItems.MightOfTheRevenant, RelevantItems.MightOfTheRevenant, DamageItem);
            RegisterItemProcessor(RelevantItems.BarrowStrength, RelevantItems.BarrowStrength, DamageItem);
            RegisterItemProcessor(RelevantItems.StaffofCleansing, RelevantItems.StaffofCleansing, CleansingItem,CombatActionPriority.High);

            RegisterItemProcessor(RelevantItems.GnuffsEternalRiftCrystal, RelevantItems.GnuffsEternalRiftCrystal, DamageItem);
            RegisterItemProcessor(RelevantItems.Drone, RelevantItems.Drone, DamageItem);

            RegisterItemProcessor(RelevantItems.WenWen, RelevantItems.WenWen, DamageItem);

            RegisterItemProcessor(RelevantItems.DreadlochEnduranceBooster, RelevantItems.DreadlochEnduranceBooster, EnduranceBooster, CombatActionPriority.High);
            RegisterItemProcessor(RelevantItems.DreadlochEnduranceBoosterNanomageEdition, RelevantItems.DreadlochEnduranceBoosterNanomageEdition, EnduranceBooster, CombatActionPriority.High);
            RegisterItemProcessor(RelevantItems.WitheredFlesh, RelevantItems.WitheredFlesh, WithFlesh, CombatActionPriority.High);
            RegisterItemProcessor(RelevantItems.DesecratedFlesh, RelevantItems.DesecratedFlesh, DescFlesh, CombatActionPriority.High);
            RegisterItemProcessor(RelevantItems.AssaultClassTank, RelevantItems.AssaultClassTank, AssaultClass, CombatActionPriority.High);

            RegisterItemProcessor(RelevantItems.MeteoriteSpikes, RelevantItems.MeteoriteSpikes, TargetedDamageItem);
            RegisterItemProcessor(RelevantItems.LavaCapsule, RelevantItems.LavaCapsule, TargetedDamageItem);
            RegisterItemProcessor(RelevantItems.HSRLow, RelevantItems.HSRHigh, TargetedDamageItem);
            RegisterItemProcessor(RelevantItems.KizzermoleGumboil, RelevantItems.KizzermoleGumboil, TargetedDamageItem);

            RegisterItemProcessor(RelevantItems.UponAWaveOfSummerLow, RelevantItems.UponAWaveOfSummerHigh, TargetedDamageItem);
            RegisterItemProcessor(RelevantItems.BlessedWithThunderLow, RelevantItems.BlessedWithThunderHigh, TargetedDamageItem);

            RegisterItemProcessor(new int[] { RelevantItems.RezCan1, RelevantItems.RezCan2 }, RezCan);
            RegisterItemProcessor(new int[] { RelevantItems.ExpCan1, RelevantItems.ExpCan2 }, ExpCan);
            RegisterItemProcessor(new int[] { RelevantItems.InsuranceCan1, RelevantItems.InsuranceCan2 }, InsuranceCan);

            RegisterItemProcessor(new int[] { RelevantItems.HealthAndNanoStim1, RelevantItems.HealthAndNanoStim200, 
            RelevantItems.HealthAndNanoStim400, }, HealthAndNanoStim, CombatActionPriority.High);

            RegisterItemProcessor(new int[] { RelevantItems.PremSitKit, RelevantItems.AreteSitKit, RelevantItems.SitKit1,
            RelevantItems.SitKit100, RelevantItems.SitKit200, RelevantItems.SitKit300, RelevantItems.SitKit400 }, SitKit);

            RegisterItemProcessor(new int[] { RelevantItems.DaTaunterLow, RelevantItems.DaTaunterHigh }, TargetedDamageItem);

            RegisterItemProcessor(new int[] { RelevantItems.FreeStim1, RelevantItems.FreeStim50, RelevantItems.FreeStim100,
            RelevantItems.FreeStim200, RelevantItems.FreeStim300 }, FreeStim);


            RegisterItemProcessor(RelevantItems.AmmoBoxArrows, RelevantItems.AmmoBoxArrows, AmmoBoxArrows);
            RegisterItemProcessor(RelevantItems.AmmoBoxBullets, RelevantItems.AmmoBoxBullets, AmmoBoxBullets);
            RegisterItemProcessor(RelevantItems.AmmoBoxEnergy, RelevantItems.AmmoBoxEnergy, AmmoBoxEnergy);
            RegisterItemProcessor(RelevantItems.AmmoBoxShotgun, RelevantItems.AmmoBoxShotgun, AmmoBoxShotgun);

            RegisterSpellProcessor(RelevantNanos.CompositeNano, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.CompositeAttribute, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.CompositeUtility, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.CompositeMartialProwess, GenericBuff);

            RegisterSpellProcessor(RelevantNanos.InsightIntoSL, GenericBuff);

            if (GetWieldedWeapons(DynelManager.LocalPlayer).HasFlag(CharacterWieldedWeapon.Melee))
            {
                //We are melee
                RegisterSpellProcessor(RelevantNanos.CompositeMartial, GenericBuffExcludeInnerSanctum);
                RegisterSpellProcessor(RelevantNanos.CompositeMelee, GenericBuff);
                RegisterSpellProcessor(RelevantNanos.CompositePhysicalSpecial, GenericBuff);
            }


            if (GetWieldedWeapons(DynelManager.LocalPlayer).HasFlag(CharacterWieldedWeapon.Ranged))
            {
                //We are ranged
                RegisterSpellProcessor(RelevantNanos.CompositeRanged, GenericBuff);
                RegisterSpellProcessor(RelevantNanos.CompositeRangedSpecial, GenericBuff);
            }

            Game.TeleportEnded += OnZoned;
            //Team.TeamRequest += Team_TeamRequest;
            //Network.N3MessageSent += Network_N3MessageSent;
        }

        //public static bool IsRaidEnabled(string[] param)
        //{
        //    return param.Length > 0 && "raid".Equals(param[0]);
        //}

        //public static Identity[] GetRegisteredCharactersInvite()
        //{
        //    Identity[] registeredCharacters = SettingsController.GetRegisteredCharacters();
        //    int firstTeamCount = registeredCharacters.Length > 6 ? 6 : registeredCharacters.Length;
        //    Identity[] firstTeamCharacters = new Identity[firstTeamCount];
        //    Array.Copy(registeredCharacters, firstTeamCharacters, firstTeamCount);
        //    return firstTeamCharacters;
        //}

        //public static Identity[] GetRemainingRegisteredCharacters()
        //{
        //    Identity[] registeredCharacters = SettingsController.GetRegisteredCharacters();
        //    int characterCount = registeredCharacters.Length - 6;
        //    Identity[] remainingCharacters = new Identity[characterCount];
        //    if (characterCount > 0)
        //    {
        //        Array.Copy(registeredCharacters, 6, remainingCharacters, 0, characterCount);
        //    }
        //    return remainingCharacters;
        //}

        //public static void SendTeamInvite(Identity[] targets)
        //{
        //    foreach (Identity target in targets)
        //    {
        //        if (target != DynelManager.LocalPlayer.Identity)
        //            Team.Invite(target);
        //    }
        //}

        //public static void Team_TeamRequest(object s, TeamRequestEventArgs e)
        //{
        //    if (SettingsController.IsCharacterRegistered(e.Requester))
        //    {
        //        e.Accept();
        //    }
        //}

        //public static void Network_N3MessageSent(object s, N3Message n3Msg)
        //{
        //    if (IsActiveWindow || n3Msg.Identity != DynelManager.LocalPlayer.Identity) { return; }

        //    //Chat.WriteLine($"{n3Msg.Identity != DynelManager.LocalPlayer.Identity}");

        //    if (n3Msg.N3MessageType == N3MessageType.LookAt)
        //    {
        //        LookAtMessage lookAtMsg = (LookAtMessage)n3Msg;
        //        IPCChannel.Broadcast(new TargetMessage()
        //        {
        //            Target = lookAtMsg.Target
        //        });
        //    }
        //    else if (n3Msg.N3MessageType == N3MessageType.Attack)
        //    {
        //        AttackMessage attackMsg = (AttackMessage)n3Msg;
        //        IPCChannel.Broadcast(new AttackIPCMessage()
        //        {
        //            Target = attackMsg.Target
        //        });
        //    }
        //    else if (n3Msg.N3MessageType == N3MessageType.StopFight)
        //    {
        //        StopFightMessage stopAttackMsg = (StopFightMessage)n3Msg;
        //        IPCChannel.Broadcast(new StopAttackIPCMessage());
        //    }
        //}

        //public static void OnDisband(int sender, IPCMessage msg)
        //{
        //    Team.Leave();
        //}


        //public static void OnStopAttackMessage(int sender, IPCMessage msg)
        //{
        //    if (IsActiveWindow)
        //        return;

        //    if (Game.IsZoning)
        //        return;

        //    DynelManager.LocalPlayer.StopAttack();
        //}

        //public static void DisbandCommand(string command, string[] param, ChatWindow chatWindow)
        //{
        //    Team.Disband();
        //    IPCChannel.Broadcast(new DisbandMessage());
        //}

        //public static void RaidCommand(string command, string[] param, ChatWindow chatWindow)
        //{
        //    if (Team.IsLeader)
        //        Team.ConvertToRaid();
        //    else
        //        Chat.WriteLine("Needs to be used from leader.");
        //}

        //public static void ReformCommand(string command, string[] param, ChatWindow chatWindow)
        //{
        //    Team.Disband();
        //    IPCChannel.Broadcast(new DisbandMessage());
        //    Task task = new Task(() =>
        //    {
        //        Thread.Sleep(1000);
        //        FormCommand("form", param, chatWindow);
        //    });
        //    task.Start();
        //}

        //public static void FormCommand(string command, string[] param, ChatWindow chatWindow)
        //{
        //    if (!DynelManager.LocalPlayer.IsInTeam())
        //    {
        //        SendTeamInvite(GetRegisteredCharactersInvite());

        //        if (IsRaidEnabled(param))
        //        {
        //            Task task = new Task(() =>
        //            {
        //                Thread.Sleep(1000);
        //                Team.ConvertToRaid();
        //                Thread.Sleep(1000);
        //                SendTeamInvite(GetRemainingRegisteredCharacters());
        //            });
        //            task.Start();
        //        }
        //    }
        //    else
        //    {
        //        Chat.WriteLine("Cannot form a team. Character already in team. Disband first.");
        //    }
        //}

        //public static void OnTargetMessage(int sender, IPCMessage msg)
        //{
        //    if (IsActiveWindow)
        //        return;

        //    if (Game.IsZoning)
        //        return;

        //    TargetMessage targetMsg = (TargetMessage)msg;
        //    Targeting.SetTarget(targetMsg.Target);
        //}

        //public static void OnAttackMessage(int sender, IPCMessage msg)
        //{
        //    Chat.WriteLine("yes");

        //    if (IsActiveWindow)
        //        return;

        //    if (Game.IsZoning)
        //        return;

        //    AttackIPCMessage attackMsg = (AttackIPCMessage)msg;
        //    Dynel targetDynel = DynelManager.GetDynel(attackMsg.Target);
        //    DynelManager.LocalPlayer.Attack(targetDynel, true);
        //}

        //public static void OnRemainingNCUMessage(int sender, IPCMessage msg)
        //{
        //    try
        //    {
        //        if (Game.IsZoning)
        //            return;

        //        RemainingNCUMessage ncuMessage = (RemainingNCUMessage)msg;
        //        SettingsController.RemainingNCU[ncuMessage.Character] = ncuMessage.RemainingNCU;
        //    }
        //    catch (Exception e)
        //    {
        //        Chat.WriteLine(e);
        //    }
        //}

        private void OnZoned(object s, EventArgs e)
        {
            _lastZonedTime = Time.NormalTime;
        }

        protected void RegisterSettingsWindow(string settingsName, string xmlName)
        {
            SettingsController.RegisterSettingsWindow(settingsName, pluginDir + "\\UI\\" + xmlName, _settings);
        }

        protected override void OnUpdate(float deltaTime)
        {
            SettingsController.CleanUp();

            //if (Time.NormalTime > _ncuUpdateTime + 0.5f)
            //{
            //    RemainingNCUMessage ncuMessage = RemainingNCUMessage.ForLocalPlayer();

            //    IPCChannel.Broadcast(ncuMessage);

            //    OnRemainingNCUMessage(0, ncuMessage);

            //    //Chat.WriteLine($"{SettingsController.RemainingNCU[ncuMessage.Character]}");

            //    foreach (KeyValuePair<Identity, int> kvp in SettingsController.RemainingNCU)
            //    {
            //        Chat.WriteLine($"{kvp.Key.Instance}");
            //        Chat.WriteLine($"{kvp.Value}");
            //    }

            //    _ncuUpdateTime = Time.NormalTime;
            //}



            base.OnUpdate(deltaTime);

            Buff buff = null;
            if ((buff = DynelManager.LocalPlayer.Buffs
                .Where(c => DebuffStopAttack.Contains(c.Id)).FirstOrDefault()) != null)
            {
                if (DynelManager.LocalPlayer.BlockAttack == false || DynelManager.LocalPlayer.FightingTarget!=null) // avoid text spam
                {
                    DynelManager.LocalPlayer.BlockAttack = true;
                    DynelManager.LocalPlayer.StopAttack();
                    Chat.WriteLine("Detected debuff:" + buff.Name + ". Stopping attack!");
                }
            }
            else if (DynelManager.LocalPlayer.BlockAttack == true)
            {
                DynelManager.LocalPlayer.BlockAttack = false;
                Chat.WriteLine("Debuff removed, attack can be resumed!");
            }

            if (DynelManager.LocalPlayer.IsAttacking || DynelManager.LocalPlayer.GetStat(Stat.NumFightingOpponents) > 0)
            {
                _lastCombatTime = Time.NormalTime;
            }
        }


        #region Perks

        private PerkConditionProcessor ToPerkConditionProcessor(GenericPerkConditionProcessor genericPerkConditionProcessor)
        {
            return (PerkAction perkAction, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget) => genericPerkConditionProcessor(perkAction, fightingTarget, ref actionTarget);
        }

        protected void RegisterPerkProcessors()
        {
            PerkAction.List.ForEach(perkAction => RegisterPerkAction(perkAction));
        }

        private void RegisterPerkAction(PerkAction perkAction)
        {
            GenericPerkConditionProcessor perkConditionProcessor = PerkCondtionProcessors.GetPerkConditionProcessor(perkAction);

            if (perkConditionProcessor != null)
            {
                RegisterPerkProcessor(perkAction.Hash, ToPerkConditionProcessor(perkConditionProcessor));
            }
        }

        public static bool TrollFormPerk(PerkAction perkAction, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (fightingTarget == null)
                return false;

            return true;
        }

        public static bool LegShotPerk(PerkAction perkAction, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (fightingTarget == null)
                return false;

            return true;
        }

        protected bool LEProc(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            foreach (Buff buff in DynelManager.LocalPlayer.Buffs.AsEnumerable())
            {
                if (buff.Name == perk.Name) { return false; }
            }

            return true;
        }
        private bool Limber(PerkAction perkAction, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!DynelManager.LocalPlayer.IsAttacking) { return false; }

            if (DynelManager.LocalPlayer.GetStat(Stat.NumFightingOpponents) == 0) { return false; }

            if (DynelManager.LocalPlayer.Buffs.Find(RelevantNanos.DanceOfFools, out Buff dof) && dof.RemainingTime > 12.5f) { return false; }

            // stop cycling if we haven't fought anything for over 10 minutes
            if (Time.NormalTime - _lastCombatTime > EvadeCycleTimeoutSeconds) { return false; }

            return true;
        }

        private bool DanceOfFools(PerkAction perkAction, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!DynelManager.LocalPlayer.IsAttacking) { return false; }

            if (DynelManager.LocalPlayer.GetStat(Stat.NumFightingOpponents) == 0) { return false; }

            if (!DynelManager.LocalPlayer.Buffs.Find(RelevantNanos.Limber, out Buff limber) || limber.RemainingTime > 12.5f) { return false; }

            // stop cycling if we haven't fought anything for over 10 minutes
            if (Time.NormalTime - _lastCombatTime > EvadeCycleTimeoutSeconds) { return false; }

            return true;
        }

        #endregion

        #region Instanced Logic

        protected bool BuffInitEngi(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (fightingTarget != null || !CanCast(spell)) { return false; }

            if (DynelManager.LocalPlayer.IsInTeam())
            {
                SimpleChar teamMemberWithoutBuff = DynelManager.Characters
                    .Where(c => Team.Members.Select(t => t.Identity.Instance).Contains(c.Identity.Instance))
                    .Where(c => GetWieldedWeapons(c).HasFlag(CharacterWieldedWeapon.Ranged))
                    .Where(c => SpellChecksOther(spell, spell.Nanoline, c))
                    .Where(c => c.Profession != Profession.Doctor && c.Profession != Profession.NanoTechnician)
                    .FirstOrDefault();

                if (teamMemberWithoutBuff != null)
                {
                    actionTarget.Target = teamMemberWithoutBuff;
                    actionTarget.ShouldSetTarget = true;
                    return true;
                }
            }
            else
            {
                if (SpellChecksPlayer(spell))
                {
                    actionTarget.Target = DynelManager.LocalPlayer;
                    actionTarget.ShouldSetTarget = true;
                    return true;
                }
            }

            return false;
        }

        private bool FountainOfLife(Spell spell, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actiontarget)
        {
            // Prioritize keeping ourself alive
            if (DynelManager.LocalPlayer.HealthPercent <= 30)
            {
                actiontarget.Target = DynelManager.LocalPlayer;
                return true;
            }

            // Try to keep our teammates alive if we're in a team
            if (DynelManager.LocalPlayer.IsInTeam())
            {
                SimpleChar dyingTeamMember = DynelManager.Characters
                    .Where(c => c.IsAlive)
                    .Where(c => Team.Members.Select(t => t.Identity.Instance).Contains(c.Identity.Instance))
                    .Where(c => c.HealthPercent < 30)
                    .FirstOrDefault();

                if (dyingTeamMember != null)
                {
                    actiontarget.Target = dyingTeamMember;
                    return true;
                }
            }

            return false;
        }

        #endregion

        #region Logic

        protected bool GenericBuffExcludeInnerSanctum(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (IsInsideInnerSanctum()) { return false; }

            if (DynelManager.LocalPlayer.Buffs.Contains(NanoLine.MajorEvasionBuffs)) { return false; }

            return GenericBuff(spell, fightingTarget, ref actionTarget);
        }

        protected bool AllTeamBuff(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!CanCast(spell)) { return false; }

            if (DynelManager.LocalPlayer.IsInTeam())
            {
                SimpleChar teamMemberWithoutBuff = DynelManager.Characters
                    .Where(c => Team.Members.Select(t => t.Identity.Instance).Contains(c.Identity.Instance))
                    .Where(c => SpellChecksOther(spell, spell.Nanoline, c))
                    .FirstOrDefault();

                if (teamMemberWithoutBuff != null)
                {
                    actionTarget.Target = teamMemberWithoutBuff;
                    actionTarget.ShouldSetTarget = true;
                    return true;
                }
            }

            return AllBuff(spell, fightingTarget, ref actionTarget);
        }

        protected bool AllBuff(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!CanCast(spell)) { return false; }

            if (SpellChecksPlayer(spell))
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = DynelManager.LocalPlayer;
                return true;
            }

            return false;
        }

        protected bool CombatBuff(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (fightingTarget == null || !CanCast(spell)) { return false; }

            if (SpellChecksPlayer(spell))
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = DynelManager.LocalPlayer;
                return true;
            }

            return false;
        }

        protected bool DebuffTarget(Spell spell, NanoLine nanoline, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            // Check if we are fighting and if debuffing is enabled
            if (fightingTarget == null) { return false; }

            if (SpellChecksOther(spell, nanoline, fightingTarget))
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = fightingTarget;
                return true;
            }

            return false;
        }

        protected bool ToggledDebuffTarget(string settingName, Spell spell, NanoLine nanoline, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            // Check if we are fighting and if debuffing is enabled
            if (fightingTarget == null || !IsSettingEnabled(settingName)) { return false; }

            if (SpellChecksOther(spell, nanoline, fightingTarget))
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = fightingTarget;
                return true;
            }

            return false;
        }

        protected bool ToggledDebuffOthersInCombat(string toggleName, Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!IsSettingEnabled(toggleName) || !CanCast(spell)) { return false; }

            SimpleChar debuffTarget = DynelManager.NPCs
                .Where(c => !debuffTargetsToIgnore.Contains(c.Name)) //Is not a quest target etc
                .Where(c => c.FightingTarget != null) //Is in combat
                .Where(c => !c.Buffs.Contains(301844)) // doesn't have ubt in ncu
                .Where(c => c.IsInLineOfSight)
                .Where(c => !c.Buffs.Contains(NanoLine.Mezz) && !c.Buffs.Contains(NanoLine.AOEMezz))
                .Where(c => c.DistanceFrom(DynelManager.LocalPlayer) < 30f) //Is in range for debuff (we assume weapon range == debuff range)
                .Where(c => SpellChecksOther(spell, spell.Nanoline, c)) //Needs debuff refreshed
                .OrderBy(c => c.MaxHealth)
                .FirstOrDefault();

            if (debuffTarget != null)
            {
                actionTarget = (debuffTarget, true);
                return true;
            }

            return false;
        }

        protected virtual bool TargetedDamageItem(Item item, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            actionTarget.ShouldSetTarget = true;
            return DamageItem(item, fightingTarget, ref actionTarget);
        }

        protected virtual bool ReflectGraft(Item item, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return !DynelManager.LocalPlayer.Cooldowns.ContainsKey(GetSkillLockStat(item)) && !DynelManager.LocalPlayer.Buffs.Contains(NanoLine.ReflectShield);
        }

        
        private bool CleansingItem(Item item, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (DynelManager.LocalPlayer.Cooldowns.ContainsKey(GetSkillLockStat(item))) return false;

            var buff = DynelManager.LocalPlayer.Buffs.Where(
                c => c.Nanoline == NanoLine.NanoShutdownDebuff ||
                c.Nanoline == NanoLine.InitiativeDebuffs ||
                c.Nanoline == NanoLine.Mezz ||
                c.Nanoline == NanoLine.Root ||
                c.Nanoline == NanoLine.Snare ||
                c.Nanoline == NanoLine.DOTNanotechnicianStrainA ||
                c.Nanoline == NanoLine.DOTNanotechnicianStrainB ||
                c.Nanoline == NanoLine.DOT_LineA ||
                c.Nanoline == NanoLine.DOT_LineB ||
                c.Nanoline == NanoLine.DOTStrainC
                ).FirstOrDefault();

            if (buff == null) return false;
            Chat.WriteLine("Use Staff of Cleansing");
            actionTarget.ShouldSetTarget = true;
            return true;
        }
        protected virtual bool DamageItem(Item item, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return !DynelManager.LocalPlayer.Cooldowns.ContainsKey(GetSkillLockStat(item)) && fightingTarget != null && fightingTarget.IsInAttackRange();
        }

        protected bool ToggledBuff(string settingName, Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!IsSettingEnabled(settingName)) { return false; }

            return GenericBuff(spell, fightingTarget, ref actionTarget);
        }

        protected bool TeamBuffExcludeInnerSanctum(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (IsInsideInnerSanctum()) { return false; }

            return TeamBuff(spell, fightingTarget, ref actionTarget);
        }

        protected bool TeamBuff(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (fightingTarget != null || !CanCast(spell) || spell.Name.Contains("Veteran")) { return false; }

            if (DynelManager.LocalPlayer.IsInTeam())
            {
                SimpleChar teamMemberWithoutBuff = DynelManager.Characters
                    .Where(c => Team.Members.Select(t => t.Identity.Instance).Contains(c.Identity.Instance))
                    .Where(c => SpellChecksOther(spell, spell.Nanoline, c))
                    .FirstOrDefault();

                if (teamMemberWithoutBuff != null)
                {
                    actionTarget.Target = teamMemberWithoutBuff;
                    actionTarget.ShouldSetTarget = true;
                    return true;
                }
            }

            return GenericBuff(spell, fightingTarget, ref actionTarget);
        }

        protected bool GenericBuff(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (fightingTarget != null || !CanCast(spell) || RelevantNanos.IgnoreNanos.Contains(spell.Id)) { return false; }

            if (SpellChecksPlayer(spell))
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = DynelManager.LocalPlayer;
                return true;
            }

            return false;
        }

        protected bool TeamBuffNoNTWeaponType(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget, CharacterWieldedWeapon supportedWeaponType)
        {
            if (fightingTarget != null || !CanCast(spell)) { return false; }

            if (DynelManager.LocalPlayer.IsInTeam())
            {
                SimpleChar teamMemberWithoutBuff = DynelManager.Characters
                    .Where(c => Team.Members.Select(t => t.Identity.Instance).Contains(c.Identity.Instance))
                    .Where(c => SpellChecksOther(spell, spell.Nanoline, c))
                    .Where(c => c.Profession != Profession.NanoTechnician)
                    // Combines both together
                    .Where(c => GetWieldedWeapons(c).HasFlag(supportedWeaponType))
                    .FirstOrDefault();

                if (teamMemberWithoutBuff != null)
                {
                    if (teamMemberWithoutBuff.Buffs.Contains(NanoLine.FixerSuppressorBuff) &&
                        (spell.Nanoline == NanoLine.FixerSuppressorBuff || spell.Nanoline == NanoLine.AssaultRifleBuffs)) { return false; }

                    if (teamMemberWithoutBuff.Buffs.Contains(NanoLine.PistolBuff) &&
                        spell.Nanoline == NanoLine.PistolBuff) { return false; }

                    actionTarget.Target = teamMemberWithoutBuff;
                    actionTarget.ShouldSetTarget = true;
                    return true;
                }
            }

            return false;
        }

        protected bool TeamBuffWeaponType(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget, CharacterWieldedWeapon supportedWeaponType)
        {
            if (fightingTarget != null || !CanCast(spell)) { return false; }

            if (DynelManager.LocalPlayer.IsInTeam())
            {
                SimpleChar teamMemberWithoutBuff = DynelManager.Characters
                    .Where(c => Team.Members.Select(t => t.Identity.Instance).Contains(c.Identity.Instance))
                    .Where(c => SpellChecksOther(spell, spell.Nanoline, c))
                    // Combines both together
                    .Where(c => GetWieldedWeapons(c).HasFlag(supportedWeaponType))
                    .FirstOrDefault();

                if (teamMemberWithoutBuff != null)
                {
                    if (teamMemberWithoutBuff.Buffs.Contains(NanoLine.FixerSuppressorBuff) &&
                        (spell.Nanoline == NanoLine.FixerSuppressorBuff || spell.Nanoline == NanoLine.AssaultRifleBuffs)) { return false; }

                    actionTarget.Target = teamMemberWithoutBuff;
                    actionTarget.ShouldSetTarget = true;
                    return true;
                }
            }

            return false;
        }

        protected bool BuffWeaponType(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget, CharacterWieldedWeapon supportedWeaponType)
        {
            if (fightingTarget != null || !CanCast(spell)) { return false; }

            if (SpellChecksPlayer(spell) && GetWieldedWeapons(DynelManager.LocalPlayer).HasFlag(supportedWeaponType))
            {
                actionTarget.Target = DynelManager.LocalPlayer;
                actionTarget.ShouldSetTarget = true;
                return true;
            }

            return false;
        }

        public bool IsAttackingUs(SimpleChar mob)
        {
            if (Team.IsInTeam)
            {
                return Team.Members.Select(m => m.Character != null && m.Character.Identity.Equals(mob.FightingTarget.Identity)).Any();
            }

            if (DynelManager.LocalPlayer.FightingTarget != null)
            {
                return mob.FightingTarget.Identity == DynelManager.LocalPlayer.Identity
                    && DynelManager.LocalPlayer.FightingTarget.Identity != mob.Identity;
            }

            return mob.FightingTarget.Identity == DynelManager.LocalPlayer.Identity;
                
        }
        public bool IsAttackingNPC(SimpleChar mob, string friendlyNPCName)
        {
            return mob.FightingTarget != null && mob.FightingTarget.Name.Equals(friendlyNPCName);
        }


        /*
         * returns true if you or anyone in the team is attacking @mob
         */
        public bool IsTeamAttacking(SimpleChar mob)
        {
            if (Team.IsInTeam)
            {
                return Team.Members.Where(
                    m => m.Character != null &&
                    m.Character.FightingTarget != null &&
                    m.Character.FightingTarget.Identity.Equals(mob.Identity)
                    ).Any();
            }

            return DynelManager.LocalPlayer.FightingTarget != null
                && DynelManager.LocalPlayer.FightingTarget.Identity == mob.Identity;
        }
        public bool IsNotFightingMe(SimpleChar target)
        {
            return target.IsAttacking && target.FightingTarget.Identity != DynelManager.LocalPlayer.Identity;
        }

        // expression body method / inline method   
        public static CharacterWieldedWeapon GetWieldedWeapons(SimpleChar local) => (CharacterWieldedWeapon)local.GetStat(Stat.EquippedWeapons);

        protected bool RangedBuff(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget) => BuffWeaponType(spell, fightingTarget, ref actionTarget, CharacterWieldedWeapon.Ranged);

        protected bool MeleeBuff(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget) => BuffWeaponType(spell, fightingTarget, ref actionTarget, CharacterWieldedWeapon.Melee);

        protected bool PistolSelfBuff(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
                return BuffWeaponType(spell, fightingTarget, ref actionTarget, CharacterWieldedWeapon.Pistol);
        }

        protected bool PistolMasteryBuff(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (Team.IsInTeam)
            {
                return TeamBuffNoNTWeaponType(spell, fightingTarget, ref actionTarget, CharacterWieldedWeapon.Pistol);
            }
            else
                return BuffWeaponType(spell, fightingTarget, ref actionTarget, CharacterWieldedWeapon.Pistol);
        }
        #endregion

        #region Items

        private bool RezCan(Item item, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actiontarget)
        {
            if (DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.FirstAid)) { return false; }

            actiontarget.ShouldSetTarget = false;
            return true;
        }

        private bool FreeStim(Item item, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actiontarget)
        {
            if (!DynelManager.LocalPlayer.Buffs.Contains(NanoLine.Root) && !DynelManager.LocalPlayer.Buffs.Contains(NanoLine.Snare)
                && !DynelManager.LocalPlayer.Buffs.Contains(258231)) { return false; }

            if (DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.FirstAid)) { return false; }

            actiontarget.ShouldSetTarget = true;
            return true;

        }

        protected bool TauntTool(Item item, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (fightingTarget == null || !IsSettingEnabled("UseTauntTool")) { return false; }

            if (TauntTools.CanUseTauntTool())
            {
                actionTarget.Target = fightingTarget;
                actionTarget.ShouldSetTarget = true;
            }

            return false;
        }

        private bool InsuranceCan(Item item, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.FirstAid)
                || DynelManager.LocalPlayer.GetStat(Stat.UnsavedXP) == 0
                || DynelManager.LocalPlayer.Buffs.Contains(300727)) { return false; }

            actionTarget.Target = DynelManager.LocalPlayer;
            actionTarget.ShouldSetTarget = false;
            return true;

        }

        private bool ExpCan(Item item, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.FirstAid)) { return false; }

            actionTarget.Target = DynelManager.LocalPlayer;
            actionTarget.ShouldSetTarget = false;
            return true;

        }

        private bool FlowerOfLife(Item item, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actiontarget)
        {
            if (fightingtarget == null) { return false; }

            if (DynelManager.LocalPlayer.Cooldowns.ContainsKey(GetSkillLockStat(item))) { return false; }

            int approximateHealing = item.QualityLevel * 10;

            return DynelManager.LocalPlayer.MissingHealth > approximateHealing;
        }

        private bool SitKit(Item item, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Treatment)) { return false; }

            if (DynelManager.LocalPlayer.HealthPercent >= 66 && DynelManager.LocalPlayer.NanoPercent >= 66) { return false; }

            actionTarget.Target = DynelManager.LocalPlayer;
            actionTarget.ShouldSetTarget = true;
            return true;
        }

        private bool HealthAndNanoStim(Item item, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actiontarget)
        {
            if (DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.FirstAid)) { return false; }

            if (DynelManager.LocalPlayer.GetStat(Stat.TemporarySkillReduction) >= 8) { return false; }

            if (DynelManager.LocalPlayer.Buffs.Contains(280470) || DynelManager.LocalPlayer.Buffs.Contains(258231)) { return false; }

            int targetHealing = item.UseModifiers
                .Where(x => x is SpellData.Healing hx && hx.ApplyOn == SpellModifierTarget.Target)
                .Cast<SpellData.Healing>()
                .Sum(x => x.Average);

            if (DynelManager.LocalPlayer.MissingHealth >= targetHealing || DynelManager.LocalPlayer.MissingNano >= targetHealing)
            {
                actiontarget.ShouldSetTarget = true;
                actiontarget.Target = DynelManager.LocalPlayer;

                return true;
            }

            return false;
        }

        private bool AmmoBoxBullets(Item item, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actiontarget)
        {
            Item bulletsammo = Inventory.Items
                .Where(c => c.Name.Contains("Bullets"))
                .Where(c => !c.Name.Contains("Crate"))
                .FirstOrDefault();

            actiontarget.ShouldSetTarget = false;

            return bulletsammo == null && !DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.WeaponSmithing);
        }

        private bool AmmoBoxEnergy(Item item, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actiontarget)
        {
            Item energyammo = Inventory.Items
                .Where(c => c.Name.Contains("Energy"))
                .Where(c => !c.Name.Contains("Crate"))
                .FirstOrDefault();

            actiontarget.ShouldSetTarget = false;

            return energyammo == null && !DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.WeaponSmithing);
        }

        private bool AmmoBoxShotgun(Item item, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actiontarget)
        {
            Item shotgunammo = Inventory.Items
                .Where(c => c.Name.Contains("Shotgun"))
                .Where(c => !c.Name.Contains("Crate"))
                .FirstOrDefault();

            actiontarget.ShouldSetTarget = false;

            return shotgunammo == null && !DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.WeaponSmithing);
        }

        private bool AmmoBoxArrows(Item item, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actiontarget)
        {
            Item arrowammo = Inventory.Items
                .Where(c => c.Name.Contains("Arrows"))
                .Where(c => !c.Name.Contains("Crate"))
                .FirstOrDefault();

            actiontarget.ShouldSetTarget = false;

            return arrowammo == null && !DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.WeaponSmithing);
        }

        private bool EnduranceBooster(Item item, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actiontarget)
        {
            if (Inventory.Find(305476, 305476, out Item absorbdesflesh))
            {
                if (!DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.BodyDevelopment)) { return false; }
            }
            if (Inventory.Find(204698, 204698, out Item absorbwithflesh))
            {
                if (!DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.BodyDevelopment)) { return false; }
            }
            if (Inventory.Find(156576, 156576, out Item absorbassaultclass))
            {
                if (!DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.DuckExp)) { return false; }
            }

            // don't use if skill is locked (we will add this dynamically later)
            if (DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Strength)) { return false; }

            // don't use if we're above 40%
            if (DynelManager.LocalPlayer.HealthPercent >= 65) { return false; }

            // don't use if nothing is fighting us
            if (DynelManager.LocalPlayer.GetStat(Stat.NumFightingOpponents) == 0) { return false; }

            // don't use if we have another major absorb running
            // we could check remaining absorb stat to be slightly more effective
            if (DynelManager.LocalPlayer.Buffs.Contains(NanoLine.BioCocoon)) { return false; }

            return true;
        }

        private bool AssaultClass(Item item, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actiontarget)
        {
            // don't use if skill is locked (we will add this dynamically later)
            if (DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.DuckExp)) { return false; }

            // don't use if we're above 40%
            if (DynelManager.LocalPlayer.HealthPercent >= 65) { return false; }

            // don't use if nothing is fighting us
            if (DynelManager.LocalPlayer.GetStat(Stat.NumFightingOpponents) == 0) { return false; }

            // don't use if we have another major absorb running
            // we could check remaining absorb stat to be slightly more effective
            if (DynelManager.LocalPlayer.Buffs.Contains(NanoLine.BioCocoon)) { return false; }

            return true;
        }

        private bool DescFlesh(Item item, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actiontarget)
        {
            if (Inventory.Find(267168, 267168, out Item enduranceabsorbenf))
            {
                if (!DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Strength)) { return false; }
            }
            if (Inventory.Find(267167, 267167, out Item enduranceabsorbnanomage))
            {
                if (!DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Strength)) { return false; }
            }
            if (Inventory.Find(156576, 156576, out Item absorbassaultclass))
            {
                if (!DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.DuckExp)) { return false; }
            }

            // don't use if skill is locked (we will add this dynamically later)
            if (DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.BodyDevelopment)) { return false; }

            // don't use if we're above 40%
            if (DynelManager.LocalPlayer.HealthPercent >= 65) { return false; }

            // don't use if nothing is fighting us
            if (DynelManager.LocalPlayer.GetStat(Stat.NumFightingOpponents) == 0) { return false; }

            // don't use if we have another major absorb running
            // we could check remaining absorb stat to be slightly more effective
            if (DynelManager.LocalPlayer.Buffs.Contains(NanoLine.BioCocoon)) { return false; }

            return true;
        }

        private bool WithFlesh(Item item, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actiontarget)
        {
            Inventory.Find(305476, 305476, out Item absorbdesflesh);

            if (Inventory.Find(267168, 267168, out Item enduranceabsorbenf))
            {
                if (!DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Strength)) { return false; }
            }
            if (Inventory.Find(267167, 267167, out Item enduranceabsorbnanomage))
            {
                if (!DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Strength)) { return false; }
            }
            if (Inventory.Find(156576, 156576, out Item absorbassaultclass))
            {
                if (!DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.DuckExp)) { return false; }
            }

            // don't use if skill is locked (we will add this dynamically later)
            if (DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.BodyDevelopment)) { return false; }

            // don't use if we're above 40%
            if (DynelManager.LocalPlayer.HealthPercent >= 65) { return false; }

            // don't use if nothing is fighting us
            if (DynelManager.LocalPlayer.GetStat(Stat.NumFightingOpponents) == 0) { return false; }

            // don't use if we have another major absorb running
            // we could check remaining absorb stat to be slightly more effective
            if (DynelManager.LocalPlayer.Buffs.Contains(NanoLine.BioCocoon)) { return false; }

            if (absorbdesflesh != null) { return false; }

            return true;
        }

        protected virtual bool Coffee(Item item, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!DynelManager.LocalPlayer.Buffs.Contains(NanoLine.FoodandDrinkBuffs))
                return DamageItem(item, fightingTarget, ref actionTarget);

            return false;
        }
        #endregion

        #region Pets

        protected bool NoShellPetSpawner(PetType petType, Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!CanSpawnPets(petType)) { return false; }

            actionTarget.ShouldSetTarget = false;
            return true;
        }

        protected bool PetSpawner(Dictionary<int, PetSpellData> petData, Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!petData.ContainsKey(spell.Id)) { return false; }

            PetType petType = petData[spell.Id].PetType;

            //Ignore spell if we already have the shell in our inventory
            if (Inventory.Find(petData[spell.Id].ShellId, out Item shell)) { return false; }

            return NoShellPetSpawner(petType, spell, fightingTarget, ref actionTarget);
        }

        protected virtual bool PetSpawnerItem(Dictionary<int, PetSpellData> petData, Item item, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!IsSettingEnabled("SpawnPets")) { return false; }

            if (!CanLookupPetsAfterZone()) { return false; }

            if (!petData.Values.Any(x => (x.ShellId == item.LowId || x.ShellId == item.HighId) && !DynelManager.LocalPlayer.Pets.Any(p => p.Type == x.PetType))) { return false; }

            actionTarget.ShouldSetTarget = false;
            return true;
        }

        protected bool CanSpawnPets(PetType petType)
        {
            if (!IsSettingEnabled("SpawnPets") || !CanLookupPetsAfterZone() || PetAlreadySpawned(petType)) { return false; }

            return true;
        }

        private bool PetAlreadySpawned(PetType petType)
        {
            return DynelManager.LocalPlayer.Pets.Any(x => (x.Type == PetType.Unknown || x.Type == petType));
        }

        protected Pet FindAttackPetThat(Func<Pet, bool> Filter)
        {
            return DynelManager.LocalPlayer.Pets
                .Where(pet => pet.Character != null && pet.Character.Buffs != null)
                .Where(pet => pet.Type == PetType.Attack)
                .Where(pet => Filter.Invoke(pet))
                .FirstOrDefault();
        }

        protected Pet FindSupportPetThat(Func<Pet, bool> Filter)
        {
            return DynelManager.LocalPlayer.Pets
                .Where(pet => pet.Character != null && pet.Character.Buffs != null)
                .Where(pet => pet.Type == PetType.Support)
                .Where(pet => Filter.Invoke(pet))
                .FirstOrDefault();
        }

        protected Pet FindPetThat(Func<Pet, bool> Filter)
        {
            return DynelManager.LocalPlayer.Pets
                .Where(pet => pet.Character != null && pet.Character.Buffs != null)
                .Where(pet => pet.Type == PetType.Support || pet.Type == PetType.Attack || pet.Type == PetType.Heal)
                .Where(pet => Filter.Invoke(pet))
                .FirstOrDefault();
        }

        protected Pet FindPets()
        {
            return DynelManager.LocalPlayer.Pets
                .Where(pet => pet.Character != null && pet.Character.Buffs != null)
                .Where(pet => pet.Type == PetType.Support || pet.Type == PetType.Attack || pet.Type == PetType.Heal)
                .FirstOrDefault();
        }
        protected Pet FindPetsWithoutBuff(int[] buff)
        {
            return DynelManager.LocalPlayer.Pets
                .Where(pet => pet.Character != null && pet.Character.Buffs != null)
                .Where(pet => pet.Type == PetType.Support || pet.Type == PetType.Attack || pet.Type == PetType.Heal)
                .Where(pet => !pet.Character.Buffs.Contains(buff))
                .FirstOrDefault();
        }

        protected Pet FindPetNeedsHeal(int percent)
        {
            return DynelManager.LocalPlayer.Pets
                .Where(pet => pet.Character != null && pet.Character.Buffs != null)
                .Where(pet => pet.Type == PetType.Support || pet.Type == PetType.Attack || pet.Type == PetType.Heal)
                .Where(pet => pet.Character.HealthPercent <= percent)
                .FirstOrDefault();
        }

        protected bool CanLookupPetsAfterZone()
        {
            return Time.NormalTime > _lastZonedTime + PostZonePetCheckBuffer;
        }

        protected bool PetTargetBuff(NanoLine buffNanoLine, PetType petType, Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!IsSettingEnabled("BuffPets") || !CanLookupPetsAfterZone()) { return false; }

            Pet petToBuff = FindPetThat(pet => pet.Type == petType && !pet.Character.Buffs.Contains(buffNanoLine));

            if (petToBuff != null)
            {
                if (!petToBuff.Character.IsAlive) { return false; }

                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = petToBuff.Character;
                return true;
            }

            return false;
        }

        protected void SynchronizePetCombatStateWithOwner()
        {
            if (CanLookupPetsAfterZone() && Time.NormalTime - _lastPetSyncTime > 1)
            {
                SynchronizePetCombatState(FindPetThat(pet => pet.Type == PetType.Attack));
                SynchronizePetCombatState(FindPetThat(pet => pet.Type == PetType.Support));

                _lastPetSyncTime = Time.NormalTime;
            }
        }

        private void SynchronizePetCombatState(Pet pet)
        {
            if (pet != null)
            {
                if (DynelManager.LocalPlayer.IsAttacking && DynelManager.LocalPlayer.FightingTarget != null)
                {
                    if (pet.Character.IsAttacking && pet.Character.FightingTarget != null
                        && pet.Character.FightingTarget.Identity != DynelManager.LocalPlayer.FightingTarget.Identity)
                    {
                        pet.Attack(DynelManager.LocalPlayer.FightingTarget.Identity);
                    }
                    else if (!pet.Character.IsAttacking)
                    {
                        pet.Attack(DynelManager.LocalPlayer.FightingTarget.Identity);
                    }
                }
                else if (!DynelManager.LocalPlayer.IsAttacking)
                {
                    if (pet.Character.IsAttacking)
                    {
                        pet.Follow();
                    }
                }
            }
        }

        #endregion

        #region Checks

        protected bool HasBuffNanoLine(NanoLine nanoLine, SimpleChar target)
        {
            return target.Buffs.Contains(nanoLine);
        }

        protected bool CheckNotProfsBeforeCast(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!CanCast(spell)) { return false; }

            if (DynelManager.LocalPlayer.IsInTeam())
            {
                SimpleChar teamMemberWithoutBuff = DynelManager.Characters
                    .Where(c => Team.Members.Select(t => t.Identity.Instance).Contains(c.Identity.Instance))
                    .Where(c => c.Profession != Profession.Keeper)
                    .Where(c => c.Profession != Profession.Engineer)
                    .Where(c => SpellChecksOther(spell, spell.Nanoline, c))
                    .FirstOrDefault();

                if (teamMemberWithoutBuff != null && teamMemberWithoutBuff.Profession != Profession.Keeper)
                {
                    actionTarget.Target = teamMemberWithoutBuff;
                    actionTarget.ShouldSetTarget = true;
                    return true;
                }
            }

            return false;
        }

        protected bool SomeoneNeedsHealing()
        {
            if (DynelManager.LocalPlayer.HealthPercent <= 85)
            {
                return true;
            }

            if (DynelManager.LocalPlayer.IsInTeam())
            {
                SimpleChar dyingTeamMember = DynelManager.Characters
                    .Where(c => c.IsAlive)
                    .Where(c => Team.Members.Select(t => t.Identity.Instance).Contains(c.Identity.Instance))
                    .Where(c => c.HealthPercent <= 85)
                    .OrderBy(c => c.Profession == Profession.Doctor)
                    .OrderBy(c => c.Profession == Profession.Enforcer)
                    .FirstOrDefault();

                if (dyingTeamMember != null)
                {
                    return true;
                }
            }

            return false;
        }

        protected bool FindMemberWithHealthBelow(int healthPercentTreshold, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (DynelManager.LocalPlayer.HealthPercent <= healthPercentTreshold)
            {
                actionTarget.Target = DynelManager.LocalPlayer;
                return true;
            }

            if (DynelManager.LocalPlayer.IsInTeam())
            {
                SimpleChar dyingTeamMember = DynelManager.Characters
                    .Where(c => Team.Members.Select(t => t.Identity.Instance).Contains(c.Identity.Instance))
                    .Where(c => c.HealthPercent <= healthPercentTreshold)
                    .OrderBy(c => c.Profession == Profession.Doctor)
                    .OrderBy(c => c.Profession == Profession.Enforcer)
                    .OrderBy(c => c.HealthPercent)
                    .FirstOrDefault();

                if (dyingTeamMember != null)
                {
                    actionTarget.Target = dyingTeamMember;
                    actionTarget.ShouldSetTarget = true;
                    return true;
                }
            }

            return false;
        }

        protected bool FindPlayerWithHealthBelow(int healthPercentTreshold, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (DynelManager.LocalPlayer.HealthPercent <= healthPercentTreshold)
            {
                actionTarget.Target = DynelManager.LocalPlayer;
                return true;
            }

            SimpleChar dyingTeamMember = DynelManager.Characters
                .Where(c => c.IsPlayer)
                .Where(c => c.HealthPercent <= healthPercentTreshold)
                .Where(c => c.DistanceFrom(DynelManager.LocalPlayer) < 30f)
                .OrderBy(c => c.Profession == Profession.Doctor)
                .OrderBy(c => c.Profession == Profession.Enforcer)
                .FirstOrDefault();

            if (dyingTeamMember != null)
            {
                actionTarget.Target = dyingTeamMember;
                actionTarget.ShouldSetTarget = true;
                return true;
            }

            return false;
        }

        protected bool HasNano(int nanoId)
        {
            return Spell.Find(nanoId, out Spell spell);
        }

        protected bool SpellChecksOther(Spell spell, NanoLine nanoline, SimpleChar fightingTarget)
        {
            if (fightingTarget == null) { return false; }
            if (spell == null) { return false; }

            if (DynelManager.LocalPlayer.Nano < spell.Cost) { return false; }

            if (Playfield.ModelIdentity.Instance == 152) { return false; }

            //if (fightingTarget.IsPlayer && !SettingsController.IsCharacterRegistered(fightingTarget.Identity)) { return false; }

            if (fightingTarget.Buffs.Find(nanoline, out Buff buff) && buff!=null)
            {

                //Don't cast if weaker than existing
                if (spell.StackingOrder <= buff.StackingOrder) { return false; }

                //THIS ONLY WORKS FOR OTHER CHARS LOADED WITH AOSHARP ON SAME PC!!! 
                // all other return 0 ncu free, and will not be buffed
                if (fightingTarget.IsPlayer && !HasNCU(spell, fightingTarget) ) { return false; }

                return true;
            }
            else
            {
                //THIS ONLY WORKS FOR OTHER CHARS LOADED WITH AOSHARP ON SAME PC!!!
                // all other return 0 ncu free, and will not be buffed
                if (fightingTarget.IsPlayer && !HasNCU(spell, fightingTarget)) { return false; }

                return true;
            }
        }

        protected bool SpellChecksPlayer(Spell spell)
        {
            if (DynelManager.LocalPlayer.Nano < spell.Cost) { return false; }

            if (Playfield.ModelIdentity.Instance == 152) { return false; }

            if (DynelManager.LocalPlayer.Buffs.Find(spell.Nanoline, out Buff buff))
            {
                //Don't cast if weaker than existing
                if (spell.StackingOrder <= buff.StackingOrder) { return false; }

                if (DynelManager.LocalPlayer.RemainingNCU < Math.Abs(spell.NCU - buff.NCU)) { return false; }

                //if (DynelManager.LocalPlayer.RemainingNCU < Math.Abs(spell.NCU - buff.NCU)) { return false; }

                return true;
            }
            else
            {
                // False profession nanos have a very high NCU usage, even if they dont take any actual NCU when cast
                // since they are hostile, so bypass the check for free NCU in that case.
                return spell.Nanoline == NanoLine.FalseProfession || DynelManager.LocalPlayer.RemainingNCU >= spell.NCU;
            }
        }

        protected bool CanCast(Spell spell)
        {
            return spell.Cost < DynelManager.LocalPlayer.Nano;
        }

        public static void CancelBuffs(int[] buffsToCancel)
        {
            foreach (Buff buff in DynelManager.LocalPlayer.Buffs)
            {
                if (buffsToCancel.Contains(buff.Id))
                    buff.Remove();
            }
        }

        protected bool IsSettingEnabled(string settingName)
        {
            return _settings[settingName]!=null && _settings[settingName].AsBool();
        }

        protected bool HasNCU(Spell spell, SimpleChar target)
        {
            return SettingsController.GetRemainingNCU(target.Identity) > spell.NCU;
        }

        private void TeleportEnded(object sender, EventArgs e)
        {
            _lastCombatTime = double.MinValue;
        }

        protected void CancelHostileAuras(int[] auras)
        {
            if (Time.NormalTime - _lastCombatTime > 5)
            {
                CancelBuffs(auras);
            }
        }

        protected bool IsInsideInnerSanctum()
        {
            return DynelManager.LocalPlayer.Buffs.Any(buff => buff.Id == RelevantNanos.InnerSanctumDebuff);
        }

        #endregion

        #region Misc

        [Flags]
        public enum CharacterWieldedWeapon
        {
            Fists = 0x0,            // 0x00000000000000000000b Fists / invalid
            MartialArts = 0x01,             // 0x00000000000000000001b martialarts / fists
            Melee = 0x02,             // 0x00000000000000000010b
            Ranged = 0x04,            // 0x00000000000000000100b
            Bow = 0x08,               // 0x00000000000000001000b
            Smg = 0x10,               // 0x00000000000000010000b
            Edged1H = 0x20,           // 0x00000000000000100000b
            Blunt1H = 0x40,           // 0x00000000000001000000b
            Edged2H = 0x80,           // 0x00000000000010000000b
            Blunt2H = 0x100,          // 0x00000000000100000000b
            Piercing = 0x200,         // 0x00000000001000000000b
            Pistol = 0x400,           // 0x00000000010000000000b
            AssaultRifle = 0x800,     // 0x00000000100000000000b
            Rifle = 0x1000,           // 0x00000001000000000000b
            Shotgun = 0x2000,         // 0x00000010000000000000b
            Grenade = 0x8000,     // 0x00000100000000000000b // 0x00001000000000000000b grenade / martial arts
            MeleeEnergy = 0x4000,     // 0x00001000000000000000b // 0x00000100000000000000b
            RangedEnergy = 0x10000,   // 0x00010000000000000000b
            Grenade2 = 0x20000,        // 0x00100000000000000000b
            HeavyWeapons = 0x40000,   // 0x01000000000000000000b
        }

        // This will eventually be done dynamically but for now I will implement
        // it statically so we can have it functional
        private Stat GetSkillLockStat(Item item)
        {
            switch (item.HighId)
            {
                case RelevantItems.ReflectGraft:
                    return Stat.SpaceTime;
                case RelevantItems.UponAWaveOfSummerLow:
                case RelevantItems.UponAWaveOfSummerHigh:
                    return Stat.Riposte;
                case RelevantItems.FlowerOfLifeLow:
                case RelevantItems.FlowerOfLifeHigh:
                case RelevantItems.BlessedWithThunderLow:
                case RelevantItems.BlessedWithThunderHigh:
                    return Stat.MartialArts;
                case RelevantItems.FlurryOfBlowsLow:
                case RelevantItems.FlurryOfBlowsHigh:
                    return Stat.AggDef;
                case RelevantItems.StrengthOfTheImmortal:
                case RelevantItems.MightOfTheRevenant:
                case RelevantItems.BarrowStrength:
                    return Stat.Strength;
                case RelevantItems.MeteoriteSpikes:
                case RelevantItems.LavaCapsule:
                case RelevantItems.KizzermoleGumboil:
                    return Stat.SharpObject;
                case RelevantItems.SteamingHotCupOfEnhancedCoffee:
                    return Stat.RunSpeed;
                case RelevantItems.GnuffsEternalRiftCrystal:
                    return Stat.MapNavigation;
                case RelevantItems.Drone:
                    return Stat.MaterialCreation;
                case RelevantItems.WenWen:
                    return Stat.RangedEnergy;
                case RelevantItems.DaTaunterLow:
                case RelevantItems.DaTaunterHigh:
                    return Stat.Psychology;
                case RelevantItems.HSRLow:
                case RelevantItems.HSRHigh:
                    return Stat.Grenade;
                case RelevantItems.StaffofCleansing:
                    return Stat.NanoPool;
                default:
                    throw new Exception($"No skill lock stat defined for item id {item.HighId}");
            }
        }


        private static class RelevantItems
        {
            public const int ReflectGraft = 95225;
            public const int FlurryOfBlowsLow = 85907;
            public const int FlurryOfBlowsHigh = 85908;
            public const int StrengthOfTheImmortal = 305478;
            public const int MightOfTheRevenant = 206013;
            public const int BarrowStrength = 204653;
            public const int StaffofCleansing = 303465;
            public const int LavaCapsule = 245990;
            public const int WitheredFlesh = 204698;
            public const int DesecratedFlesh = 305476;
            public const int AssaultClassTank = 156576;
            public const int HSRLow = 164780;
            public const int HSRHigh = 164781;
            public const int KizzermoleGumboil = 245323;
            public const int SteamingHotCupOfEnhancedCoffee = 157296;
            public const int DreadlochEnduranceBooster = 267168;
            public const int DreadlochEnduranceBoosterNanomageEdition = 267167;
            public const int MeteoriteSpikes = 244204;
            public const int FlowerOfLifeLow = 70614;
            public const int FlowerOfLifeHigh = 204326;
            public const int UponAWaveOfSummerLow = 205405;
            public const int UponAWaveOfSummerHigh = 205406;
            public const int BlessedWithThunderLow = 70612;
            public const int BlessedWithThunderHigh = 204327;
            public const int GnuffsEternalRiftCrystal = 303179;
            public const int Drone = 303188;
            public const int RezCan1 = 301070;
            public const int RezCan2 = 303390;
            public const int ExpCan1 = 288769;
            public const int ExpCan2 = 303376;
            public const int InsuranceCan1 = 300728;
            public const int InsuranceCan2 = 303389;
            public const int PremSitKit = 297274;
            public const int AreteSitKit = 292256;
            public const int SitKit1 = 291082;
            public const int SitKit100 = 291083;
            public const int SitKit200 = 291084;
            public const int SitKit300 = 293296;
            public const int SitKit400 = 293297;
            public const int FreeStim1 = 204103;
            public const int FreeStim50 = 204104;
            public const int FreeStim100 = 204105;
            public const int FreeStim200 = 204106;
            public const int FreeStim300 = 204107;
            public const int HealthAndNanoStim1 = 291043;
            public const int HealthAndNanoStim200 = 291044;
            public const int HealthAndNanoStim400 = 291045;
            public const int AmmoBoxEnergy = 303138;
            public const int AmmoBoxShotgun = 303141;
            public const int AmmoBoxBullets = 303137;
            public const int AmmoBoxArrows = 303136;
            public const int DaTaunterLow = 158045;
            public const int DaTaunterHigh = 158046;
            public const int WenWen = 129656;
        };

        private static class RelevantNanos
        {
            public const int FountainOfLife = 302907;
            public const int DanceOfFools = 210159;
            public const int Limber = 210158;
            public const int CompositeAttribute = 223372;
            public const int CompositeNano = 223380;
            public const int CompositeUtility = 287046;
            public const int CompositeMartialProwess = 302158;
            public const int CompositeMartial = 302158;
            public const int CompositeMelee = 223360;
            public const int CompositePhysicalSpecial = 215264;
            public const int CompositeRanged = 223348;
            public const int CompositeRangedSpecial = 223364;
            public const int InnerSanctumDebuff = 206387;
            public const int InsightIntoSL = 268610;
            public static int[] IgnoreNanos = new[] { 302535, 302534, 302544, 302542, 302540, 302538, 302532, 302530 };
        }

        public class PetSpellData
        {
            public int ShellId;
            public int ShellId2;
            public PetType PetType;

            public PetSpellData(int shellId, PetType petType)
            {
                ShellId = shellId;
                PetType = petType;
            }
            public PetSpellData(int shellId, int shellId2, PetType petType)
            {
                ShellId = shellId;
                ShellId2 = shellId2;
                PetType = petType;
            }
        }
        #endregion
    }
}